FROM ruby:2.6.3-stretch

COPY . /app

RUN \
  curl -sL https://deb.nodesource.com/setup_12.x | bash - \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update \
  && apt-get install -y \
  build-essential \
  nodejs \
  yarn \
  && cd /app \
  && gem install bundler \
  && bundle install \
  && yarn install \
  && bundle exec rails assets:precompile RAILS_ENV=production \
       SECRET_KEY_BASE=invalid:secret \
       DATABASE_URL=postgresql:does_not_exist

WORKDIR /app

EXPOSE 3000

CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
